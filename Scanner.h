#ifndef __Scanner_header_
#define __Scanner_haeder_

#include<cstdio>
#include<cctype>
#include<fstream>
#include<iostream>
#include<vector>
#include <stack>

typedef enum { _id, _comen, _throw, _num, _lpar, _rpar, _lcor, _rcor,
               _suma, _resta, _mult, _div, _coma, _puncoma, _dospunt, _punto, _eof, _err, _no} token;

typedef struct{
    int generador;
    std::string p1;
    std::string p2;
    std::string p3;
}rule;

typedef std::vector<rule> grammar;

bool ll1(FILE *, grammar &, int[][15]);
std::string leerToken(FILE *);
bool isdicc(std::string );
int numGen(std::string);
int numTerm(std::string);
void llenarGramatica(grammar &);
void llenarControlador(int [][15]);

token scanner(FILE *);
bool wsp(FILE *);
token id(FILE *);
token numeros(FILE *);
token comentario(FILE *);
token unique(FILE *);
bool eof(FILE *);

int saltos();
void acepta();
void falla();
char siguiente(FILE *);
void retroceso();
void imprimir();

#endif
