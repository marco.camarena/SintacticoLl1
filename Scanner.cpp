#include "Scanner.h"

int salto = 0;
int ultimo = 0;
int pos = 0;
token Token;
int retorno;
std::string palabra;

bool ll1(FILE *fichero, grammar &G, int controlador[][15]){
    std::stack<std::string> stack;
    int pi;

    std::string x, T = leerToken(fichero);
    stack.push("main");
    do{
        if(!stack.empty()){
            x = stack.top();
            stack.pop();
        }else return false;
        if(isdicc(x)){
            if(x == T) T = leerToken(fichero);
            else return false;
        }else{
            pi = controlador[numGen(x)][numTerm(T)];
            if(pi == 0) return false;
            else{
                for(int i = 0; i <= G.size(); i++){
                    if(G[i].generador == pi){
                        if(G[i].p3 != "") stack.push(G[i].p3);
                        if(G[i].p2 != "") stack.push(G[i].p2);
                        if(G[i].p1 != "") stack.push(G[i].p1);
                        i = G.size();
                    }
                }
            }
        }
    }while(T != "EOF");
    return true;
}

std::string leerToken(FILE *fichero){
    token t;
    do{
        t = scanner(fichero);
    }while(t == _comen);

    switch(t){
        case _id:       return "identifier"; break;
        case _throw:    return "throw"; break;
        case _puncoma:  return ";"; break;
        case _lpar:     return "("; break;
        case _lcor:     return "["; break;
        case _suma:     return "+"; break;
        case _resta:    return "-"; break;
        case _mult:     return "*"; break;
        case _num:      return "numbers"; break;
        case _coma:     return ","; break;
        case _punto:    return "."; break;
        case _rpar:     return ")"; break;
        case _rcor:     return "]"; break;
        case _div:      return "/"; break;
        case _dospunt:  return ":"; break;
        case _eof:      return "EOF"; break;
        case _err:      return "error"; break;
    }
}

bool isdicc(std::string x){

    if(x == "identifier" || x == "throw" || x == ";" || x == "(" || x == "[" || x == "+" || x == "-" || x == "*" ||
       x == "numbers" || x == "," || x == "." || x == ")" || x == "]" || x == "/" || x == ":") return true;
    else return false;
}

int numGen(std::string x){
        if(x == "main")            return 1;
        if(x == "stsequence")      return 2;
        if(x == "stsequence'")     return 3;
        if(x == "statement")       return 4;
        if(x == "assignment")      return 5;
        if(x == "throwing")        return 6;
        if(x == "mexpression")     return 7;
        if(x == "mexpression'")    return 8;
        if(x == "mterm")           return 9;
        if(x == "mterm'")          return 10;
        if(x == "mfactor")         return 11;
        if(x == "mdefinition")     return 12;
        if(x == "rowsequence")     return 13;
        if(x == "rowsequence'")    return 14;
        if(x == "row")             return 15;
        if(x == "row'")            return 16;
        if(x == "expression")      return 17;
        if(x == "expression'")     return 18;
        if(x == "term")            return 19;
        if(x == "term'")           return 20;
        if(x == "factor")          return 21;
}

int numTerm(std::string t){
        if(t == "identifier")       return 1;
        if(t == "throw")            return 2;
        if(t == ";")                return 3;
        if(t == "(")                return 4;
        if(t == "[")                return 5;
        if(t == "+")                return 6;
        if(t == "-")                return 7;
        if(t == "*")                return 8;
        if(t == "numbers")          return 9;
        if(t == ",")                return 10;
        if(t == ".")                return 11;
        if(t == ")")                return 12;
        if(t == "]")                return 13;
        if(t == "/")                return 14;
}

void llenarGramatica(grammar &G){
    rule R;
    R.generador = 1;   R.p1 = "stsequence";    R.p2 = "."; R.p3 = "";
    G.push_back(R);
    R.generador = 2; R.p1 = "statement"; R.p2 = "stsequence'";   R.p3 = "";
    G.push_back(R);
    R.generador = 3;    R.p1 = ";"; R.p2 = "statement"; R.p3 = "stsequence'";
    G.push_back(R);
    R.generador = 4;    R.p1 = ""; R.p2 = "";  R.p3 = "";
    G.push_back(R);
    R.generador = 5;  R.p1 = "assignment";    R.p2 = "";  R.p3 = "";
    G.push_back(R);
    R.generador = 6;  R.p1 = "throwing";  R.p2 = "";  R.p3 = "";
    G.push_back(R);
    R.generador = 7; R.p1 = "identifier";    R.p2 = ":"; R.p3 = "mexpression";
    G.push_back(R);
    R.generador = 8;   R.p1 = "throw"; R.p2 = "mexpression";   R.p3 = "";
    G.push_back(R);
    R.generador = 9;   R.p1 = "mterm"; R.p2 = "mexpression'";   R.p3 = "";
    G.push_back(R);
    R.generador = 10;   R.p1 = "+"; R.p2 = "mterm";   R.p3 = "mexpression'";
    G.push_back(R);
    R.generador = 11;   R.p1 = "-"; R.p2 = "mterm";   R.p3 = "mexpression'";
    G.push_back(R);
    R.generador = 12;   R.p1 = ""; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 13;   R.p1 = "mfactor"; R.p2 = "mterm'";   R.p3 = "";
    G.push_back(R);
    R.generador = 14;   R.p1 = "*"; R.p2 = "mfactor";   R.p3 = "mterm'";
    G.push_back(R);
    R.generador = 15;   R.p1 = ""; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 16;   R.p1 = "("; R.p2 = "mexpression";   R.p3 = ")";
    G.push_back(R);
    R.generador = 17;   R.p1 = "mdefinition"; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 18;   R.p1 = "identifier"; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 19;   R.p1 = "["; R.p2 = "rowsequence";   R.p3 = "]";
    G.push_back(R);
    R.generador = 20;   R.p1 = "row"; R.p2 = "rowsequence'";   R.p3 = "";
    G.push_back(R);
    R.generador = 21;   R.p1 = ";"; R.p2 = "row";   R.p3 = "rowsequence'";
    G.push_back(R);
    R.generador = 22;   R.p1 = ""; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 23;   R.p1 = "expression"; R.p2 = "row'";   R.p3 = "";
    G.push_back(R);
    R.generador = 24;   R.p1 = ","; R.p2 = "expression";   R.p3 = "row'";
    G.push_back(R);
    R.generador = 25;   R.p1 = ""; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 26;   R.p1 = "term"; R.p2 = "expression'";   R.p3 = "";
    G.push_back(R);
    R.generador = 27;   R.p1 = "+"; R.p2 = "term";   R.p3 = "expression'";
    G.push_back(R);
    R.generador = 28;   R.p1 = "-"; R.p2 = "term";   R.p3 = "expression'";
    G.push_back(R);
    R.generador = 29;   R.p1 = ""; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 30;   R.p1 = "factor"; R.p2 = "term'";   R.p3 = "";
    G.push_back(R);
    R.generador = 31;   R.p1 = "*"; R.p2 = "factor";   R.p3 = "term'";
    G.push_back(R);
    R.generador = 32;   R.p1 = "/"; R.p2 = "factor";   R.p3 = "term'";
    G.push_back(R);
    R.generador = 33;   R.p1 = ""; R.p2 = "";   R.p3 = "";
    G.push_back(R);
    R.generador = 34;   R.p1 = "-"; R.p2 = "factor";   R.p3 = "";
    G.push_back(R);
    R.generador = 35;   R.p1 = "("; R.p2 = "expression";   R.p3 = ")";
    G.push_back(R);
    R.generador = 36;   R.p1 = "numbers"; R.p2 = "";   R.p3 = "";
    G.push_back(R);
}

void llenarControlador(int controlador[][15]){

    for(int i = 0; i < 22; i++)
        for(int j = 0; j < 15; j++)
            controlador[i][j] = 0;

    controlador[1][1] = 1;   controlador[1][2] = 1;
    controlador[2][1] = 2;   controlador[2][2] = 2;
    controlador[3][3] = 3;   controlador[3][11] = 4;
    controlador[4][1] = 5;   controlador[4][2] = 6;
    controlador[5][1] = 7;
    controlador[6][2] = 8;
    controlador[7][1] = 9;   controlador[7][4] = 9;   controlador[7][5] = 9;
    controlador[8][3] = 12;  controlador[8][6] = 10;  controlador[8][7] = 11;  controlador[8][11] = 12; controlador[8][12] = 12;
    controlador[9][1] = 13;  controlador[9][4] = 13;  controlador[9][5] = 13;
    controlador[10][3] = 15; controlador[10][6] = 15; controlador[10][7] = 15; controlador[10][8] = 14; controlador[10][11] = 15; controlador[10][12] = 15;
    controlador[11][1] = 18; controlador[11][4] = 16; controlador[11][5] = 17;
    controlador[12][5] = 19;
    controlador[13][4] = 20; controlador[13][7] = 20; controlador[13][9] = 20;
    controlador[14][3] = 21; controlador[14][13] = 22;
    controlador[15][4] = 23; controlador[15][7] = 23; controlador[15][9] = 23;
    controlador[16][3] = 25; controlador[16][10] = 24;controlador[16][13] = 25;
    controlador[17][4] = 26; controlador[17][7] = 26; controlador[17][9] = 26;
    controlador[18][3] = 29; controlador[18][6] = 27; controlador[18][7] = 28; controlador[18][10] = 29;controlador[18][12] = 29; controlador[18][13] = 29;
    controlador[19][4] = 30; controlador[19][7] = 30; controlador[19][9] = 30;
    controlador[20][3] = 33; controlador[20][6] = 33; controlador[20][7] = 33; controlador[20][8] = 31; controlador[20][10] = 33; controlador[20][12] = 33; controlador[20][13] = 33; controlador[20][14] = 32;
    controlador[21][4] = 35; controlador[21][7] = 34; controlador[21][9] = 36;
}

token scanner(FILE *fichero){

    while(wsp(fichero));

    Token = comentario(fichero);
    if(Token != _no) return Token;

    Token = id(fichero);
    if(Token != _no) return Token;

    Token = numeros(fichero);
    if(Token != _no) return Token;

    Token = unique(fichero);
    if(Token != _no) return Token;

    if(eof(fichero)) return _eof;

    return _err;
}

bool wsp(FILE *fichero){
    if(isspace(siguiente(fichero))){
        acepta();
        return true;
    }
    falla();
    return false;
}

token id(FILE *fichero){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 3){
        anterior = actual;
        c = siguiente(fichero);

        if(c == '\n'){
                salto++;
        }
        switch(actual){
            case 0:
                if(c == '_' || isalpha(c)) actual = 1;
                else actual = 3;
            break;

            case 1:
                if(isalnum(c) || c == '_') actual = 1;
                else if(c == 39) actual = 2;
                else actual = 3;
            break;

            case 2:
                if(c == 39) actual = 2;
                else actual = 3;
            break;
        }
        if (actual != 3) palabra.push_back(c);
    }
    if(palabra == "throw"){
        retroceso();
        acepta();
        return _throw;
    }else if(anterior == 1 || anterior == 2){
        retroceso();
        acepta();
        return _id;
    }else{
        falla();
        return _no;
    }
}

token numeros(FILE *fichero){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 12 && actual != 13){
        anterior = actual;
        c = siguiente(fichero);

        if(c == '\n'){
        salto++;
        }
        switch(actual){
            case 0:
                if(c == '0') actual = 1;
                else if(c >= 49 && c <= 57) actual = 2;
                else actual = 13;
            break;

            case 1:
                if(c >= 48 && c <= 55) actual = 3;
                else if(c == 46) actual = 5;
                else if(c == 'x' || c == 'X') actual = 4;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 2:
                if(isdigit(c)) actual = 2;
                else if(c == 46) actual = 5;
                else if(c == 'e' || c == 'E') actual = 6;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 3:
                if(c >= 48 && c <= 55) actual = 3;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 4:
                if(isdigit(c)) actual = 7;
                else if(c >= 65 && c <= 70) actual = 7;
                else if(c >= 97 && c <= 102) actual = 7;
                else actual = 13;
            break;

            case 5:
                if(isdigit(c)) actual = 8;
                else actual = 13;
            break;

            case 6:
                if(isdigit(c)) actual = 9;
                else if(c == 43 || c == 45) actual = 10;
                else actual = 13;
            break;

            case 7:
                if(isdigit(c)) actual = 11;
                else if(c >= 65 && c <= 70) actual = 11;
                else if(c >= 97 && c <= 102) actual = 11;
                else actual = 13;
            break;

            case 8:
                if(isdigit(c)) actual = 8;
                else if(c == 'e' || c == 'E') actual = 6;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 9:
                if(isdigit(c)) actual = 9;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 10:
                if(isdigit(c)) actual = 9;
                else actual = 13;
            break;

            case 11:
                if(isdigit(c)) actual = 7;
                else if(c >= 65 && c <= 70) actual = 7;
                else if(c >= 97 && c <= 102) actual = 7;
                else if((c >= 71 && c <= 90) || (c >= 103 && c <= 122)) actual = 12;
                else actual = 13;
            break;
        }
        if(actual != 13 && actual != 12) palabra.push_back(c);
    }
    if(actual != 12){
        if(anterior == 1 || anterior == 3 || anterior == 2 || anterior == 8 || anterior == 9 || anterior == 11){
            retroceso();
            acepta();
            return _num;
        }else{
            falla();
            return _no;
        }
    }else return _err;
}

token comentario(FILE *fichero){
    palabra = "";
    char c;

    c = siguiente(fichero);
    if(c == '!'){
        while(true){
            c = siguiente(fichero);
            if(c == '\n'){
                salto++;
                break;
            }
        }
        retroceso();
        acepta();
        return _comen;
    }else{
        falla();
        return _no;
    }
}

token unique(FILE *fichero){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 13){
        anterior = actual;
        c = siguiente(fichero);

        if(c == '\n'){
            salto++;
            break;
        }
        switch(actual){
            case 0:
                if(c == '(') {actual = 1; Token = _lpar;}
                else if(c == ')') {actual = 2; Token = _rpar;}
                else if(c == '[') {actual = 3; Token = _lcor;}
                else if(c == ']') {actual = 4; Token = _rcor;}
                else if(c == '+') {actual = 5; Token = _suma;}
                else if(c == '-') {actual = 6; Token = _resta;}
                else if(c == '*') {actual = 7; Token = _mult;}
                else if(c == '/') {actual = 8; Token = _div;}
                else if(c == ',') {actual = 9; Token = _coma;}
                else if(c == ';') {actual = 10; Token = _puncoma;}
                else if(c == ':') {actual = 11; Token = _dospunt;}
                else if(c == '.') {actual = 12; Token = _punto;}
                else actual = 13;
            break;
            default:
                actual = 13;
            break;
        }
        if (actual != 13) palabra.push_back(c);
    }
    if(anterior != 13){
        retroceso();
        acepta();
        return Token;
    }else{
        falla();
        return _no;
    }
}

bool eof(FILE *fichero){
    if(siguiente(fichero) == EOF) return true;
    return false;
}

int saltos(){return salto;}

void acepta(){ultimo = pos;}

void falla(){pos = ultimo;}

char siguiente(FILE *fichero){
    char c;
    fseek(fichero, pos, SEEK_SET);
    c = fgetc(fichero);
    pos++;
    return c;
}

void retroceso(){pos--;}

void imprimir(){std::cout<<palabra;}
