#include "Scanner.h"

int main(){

    grammar G;
    int controlador[22][15];

    llenarGramatica(G);
    llenarControlador(controlador);

    FILE *fichero;

    char nombre[15];

    printf("Nombre de Archivo: ");
    std::cin >> nombre;
    printf("\n");

    fichero = fopen(nombre, "r");

    if(ll1(fichero, G, controlador)) printf("\n\nARCHIVO ACEPTADO\n\nNumero de lineas: %i\n\n", saltos());
    else printf("\n\nARCHIVO NO ACEPTADO\n\nError en linea: %i\n\n", saltos());

    fclose(fichero);
    return 0;
}
